package it.unipd.dei.ims.AlgorithmsExecution;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/** This class control the parsing of a LETOR formatted file
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it
 *  
 */
public class Letor {
	
	int queryID;
	String docNO;	
	
	static List<Letor> lines = new ArrayList<Letor>();
	

	public Letor(int qid, String docNO) {
		this.queryID = qid;
		this.docNO = docNO;
	}
	
	/**
	* Print queryId and DocId
	* @param line to be printed.
	*/
	public static void printRun(Letor line) {
		System.out.print("Query ID: " + line.queryID);
		System.out.print("Doc ID: " + line.docNO);
	}
	
	
	/**
	* Read lines in LETOR format. I am interested in qid and docno.. 
	* @param in BufferedReader for the file.
	* @param numFeatures number of features in the generated Letor files
	* @throws IOException if cannot read the runs.
	*/
	public static void readLine (BufferedReader in, int numFeatures) throws IOException {
		String line = in.readLine();
		String queryID = "";
		int qid;
		String docNO = "";
			
		while(line != null) {
			String[] splitStr = line.split("\\s+");
			queryID = splitStr[1];
			queryID = queryID.replaceAll("[^\\d.]", "");
			qid = Integer.parseInt(queryID);

			docNO = splitStr[numFeatures+4]; ///  (28 with 24 features => num_features+4) 
//			System.out.println("docno: " + docNO);
			
			Letor newLine = new Letor(qid, docNO);
			lines.add(newLine);			
			
			line=in.readLine();
		}
	}
	
	/**
	* Merge letor files according to train/validation/test division
	* @param qids The string of qids (e.g 351,352,567,..).
	* @param output_file The output file (path to output file)
	* @param run The name of the run.
	* @param usedfeatures The string representing the used features.
	*/
	public static void mergeLetor(String qids, String output_file, String run) throws IOException{
		 
			String[] trainArray = qids.split(",");

		    File output = new File(output_file);
		    Writer destination = Files.newBufferedWriter(output.toPath(), StandardCharsets.UTF_8); 

		    for(String qid: trainArray) {
			    File file = new File(Execution.out_path + "/info/" + run + "/letors/" + qid + ".letor");
			    Reader source = Files.newBufferedReader(file.toPath());
			    transfer(source, destination);

			    // to delete temporary files
			    //file.delete();
		    }
						 
		   
			destination.close();
	 }
	
	/**
	* Print a LETOR formatted line on a file
	* @param pw the PrintWriter.
	* @param qid The query ID.
	* @param docno the document DocNo.
	* @param arrayInMap The array containing the feature values 
	 * @throws IOException 
	*/
	public static void writeLineLetor(PrintWriter pw, Long position, RandomAccessFile raf, int qid, String docno) throws IOException {
		raf.seek(position); //set the raf to the desidered position
		pw.write("" + raf.readInt()); // Write relevance Jud
		pw.write(" qid:" + qid + " ");

		int tmp = 1; 

		double[] tmp_values = new double[Features.features.size()];
		
		/** Get LETOR values from disk */
		for (int i = 0; i < Features.features.size(); i++) {
			Features feature = Features.features.get(i);
			if (feature.valid == true) {
				if( feature.data_type.equalsIgnoreCase("java.lang.Integer")) {
					tmp_values[i] = raf.readInt();
				}
				else if (feature.data_type.equalsIgnoreCase("java.lang.Double")) {
					tmp_values[i] = raf.readDouble();
				}
			}
			else {
				if( feature.data_type.equalsIgnoreCase("java.lang.Integer")) {
					tmp_values[i] = raf.readInt();
				}
				else if (feature.data_type.equalsIgnoreCase("java.lang.Double")) {
					tmp_values[i] = raf.readDouble();
				}
			}
		}
		

		/** Write LETOR */
		for(int i=0; i<Features.used_features.size(); i++) { // for each used feature
			Features feat = Features.used_features.get(i);
			for(int j=0; j<Features.features.size(); j++) { // for each computed feature
				if(feat.name.equals(Features.features.get(j).name)) { // find out the index in which this feature is 
					if(feat.data_type.equals("java.lang.Integer")){
						pw.write(tmp + ":" + (int)tmp_values[j] + " ");
						tmp++;
						break;
					}
					else if (feat.data_type.equals("java.lang.Double")) {
						pw.write(tmp + ":" + tmp_values[j] + " ");
						tmp++;
						break;
					}	
				}
			}
		}
		pw.write(" #docno = " + docno);
		pw.write("\r\n");

	}
	

	/**
	* Print features intro (e.g # 1:DPH \n # 2:DocumentLength)
	*/
	public static void printFeaturesIntroLETOR(PrintWriter pw, String[] featuresNames) {
		for(int i = 0; i < featuresNames.length; i++) {
			int index = i + 1;
			pw.write("# " + index + ":" + featuresNames[i]);
			pw.write("\r\n");
		}
	}

	/**
	* Transfer chars from source to destination in efficient chunks
	* @param source The source Reader.
	* @param destination The destination Writer.
	* @throws IOException
	*/
	private static final void transfer(final Reader source, final Writer destination) throws IOException {
	     char[] buffer = new char[1024 * 32];
	     int len = 0;
	     while ((len = source.read(buffer)) >= 0) { // reads characters from source and copy to dst buffer
	         destination.write(buffer, 0, len);
	     }
	 }
	
}

