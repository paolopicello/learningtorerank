package it.unipd.dei.ims.AlgorithmsExecution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;

/** This class contains utility methods
 * 
 * @author	Paolo Picello 
 * @email	paolo.picello@studenti.unipd.it 
 * 
 */
public class Utility {

	
	/**
	* Create a temporary script named script.tmp in the default temporary-file directory
	* @param array The commands in the script
	* @return The file created.
	*/
	public static File createTempScript(String[] array) throws IOException {
	    File tempScript = File.createTempFile("script", null);

	    Writer streamWriter = new OutputStreamWriter(new FileOutputStream(tempScript));
	    PrintWriter printWriter = new PrintWriter(streamWriter);

	    for(int i = 0; i < array.length; i++) {
	    	printWriter.println(array[i]);
	    }
	    printWriter.close();
	    return tempScript;
	}

	
	/**
	* Read an hashmap from file and populate an with those values
	* @param qid The qid indicates which hashmap we're interested
	* @return The hashmap read from memory
	*/	
	public static HashMap<String,Long> readFromHashMapFile ( int qid) throws IOException, ClassNotFoundException {
		String path = Execution.pointers_path + "/" + qid;
		File toRead = new File(path);
        FileInputStream fis = new FileInputStream(toRead);
        ObjectInputStream ois = new ObjectInputStream(fis);

        @SuppressWarnings("unchecked")
		HashMap<String,Long> mapInFile = (HashMap<String,Long>)ois.readObject();

        ois.close();
        fis.close();

        return mapInFile;
	}
		
	
	/**
	* Return the number of lines in a file
	* @param filename The filename.
	* @return The number of lines.
	*/
	public static int countLines(String filename) throws IOException {
	    BufferedReader reader  = new BufferedReader(new FileReader(filename));
	    int cnt = 0;
	    String lineRead = "";
	    while ((lineRead = reader.readLine()) != null) {
	    	cnt++;
	    }
	    reader.close();
	    return cnt;
	}
	
	
	/**
	 * Returns file names in a particular directory
	 * @param path The path of the directory
	 * @return An array containing the name of the files in this directory
	 */
	public static String[] listContents (String path) {
			File file = new File(path);
			String[] files = file.list(new FilenameFilter() {

			    @Override
			    public boolean accept(File dir, String name) {
		            return !name.equals(".DS_Store"); //control for OSX .DS_Store files
			    }
			});
			return files;
	}
	
	/**
	* Delete the content of a folder.
	* @param folder The folder which content will be deleted.
	*/
	public static void deleteFolder(File folder) {
	    File[] files = folder.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	            if(f.isDirectory()) {
	                deleteFolder(f);
	            } else {
	                f.delete();
	            }
	        }
	    }
	    //folder.delete(); //uncomment this line if you want to delete also the folder
	}
	
	/**
	 * Transfers bytes from source File to dest File using FileChannels
	 * @param source Source file.
	 * @param dest Destination file.
	 * @throws IOException
	 */
	public static void copyFileUsingFileChannels(File source, File dest)
			throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}
	
	/**
	 * Return the logger Level given the level name as String
	 * @param logger_level the level as String
	 * @return
	 */
	public static Level getLoggerLevel(String logger_level) {
		// TODO Auto-generated method stub
		if(logger_level.equalsIgnoreCase("TRACE"))
			return Level.TRACE;
		else if(logger_level.equalsIgnoreCase("DEBUG"))
			return Level.DEBUG;
		else if(logger_level.equalsIgnoreCase("INFO"))
			return Level.INFO;
		else if(logger_level.equalsIgnoreCase("WARN"))
			return Level.WARN;
		else if(logger_level.equalsIgnoreCase("ERROR"))
			return Level.ERROR;
		else if(logger_level.equalsIgnoreCase("FATAL"))
			return Level.FATAL;
		else 
			return Level.OFF;
	}


	public static void configureLogger() {
		Properties properties=new Properties();
	    properties.setProperty("log4j.rootLogger","TRACE,stdout,MyFile");
	    properties.setProperty("log4j.rootCategory","TRACE");

	    properties.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
	    properties.setProperty("log4j.appender.stdout.layout",  "org.apache.log4j.PatternLayout");
	    properties.setProperty("log4j.appender.stdout.layout.ConversionPattern","%d{yyyy/MM/dd HH:mm:ss.SSS} [%1p] %t (%F) - %m%n");

	    properties.setProperty("log4j.appender.MyFile", "org.apache.log4j.RollingFileAppender");
	    properties.setProperty("log4j.appender.MyFile.File", "logs/last_execution.log"); //set log for last execution
	    properties.setProperty("log4j.appender.MyFile.MaxFileSize", "10000KB");
	    properties.setProperty("log4j.appender.MyFile.MaxBackupIndex", "1");
	    properties.setProperty("log4j.appender.MyFile.layout",  "org.apache.log4j.PatternLayout");
	    properties.setProperty("log4j.appender.MyFile.layout.ConversionPattern","%d{yyyy/MM/dd HH:mm:ss.SSS} [%1p] %t (%F) - %m%n");

	    PropertyConfigurator.configure(properties);
	}
}
