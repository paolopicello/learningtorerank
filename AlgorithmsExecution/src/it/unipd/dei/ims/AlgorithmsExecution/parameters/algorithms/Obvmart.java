// This algorithm always return 0.. TODO: Understand why..
//
//package it.unipd.dei.ims.AlgorithmsExecution.properties.algorithms;
//
//
//import java.io.File;
//import java.util.Properties;
//
//import it.unipd.dei.ims.Picello2.Execution;
//
//public class Obvmart implements ReadParameters {
//
//
//	String[] arguments_array = null;
//	String arguments = "";
//	
//	String runname = "";
//	
//	String quickrank_train_metric = "";
//	String quickrank_train_cutoff = "";
//	String quickrank_test_metric = "";
//	String quickrank_test_cutoff = "";
//	
//	String num_trees = "";
//	String num_leaves = "";
//	String shrinkage = "";
//	String min_leaf_support = "";
//	String estop = "";
//	String tree_depth ="";
//	
//	String num_thresholds = "";
//	String tc = "";
//	String norm = "";
//	
//	
//	@Override
//	public String[] gen_cmdline(Properties prop, String algo, String library,String runname) {
//	
//				// Get my train, validation and test files...
//		String train_file = Execution.out_path + "/"  + runname + "/" + runname + "_TRAIN" + ".letor";
//		String validation_file = Execution.out_path + "/"  + runname + "/" + runname + "_VALIDATION" + ".letor";
//		String test_file = Execution.out_path + "/"  + runname + "/"  + runname + "_TEST" + ".letor";
//		String score_file = Execution.out_path + "/"  + runname + "/" + runname + "_scores_" + library + ".txt";  
//				
//				// Get absolute paths to those files..
//				File file_train = new File(train_file);
//				String train_path = file_train.getAbsolutePath();
//				File file_vali = new File(validation_file);
//				String validation_path= file_vali.getAbsolutePath();
//				File file_test = new File(test_file);
//				String test_path = file_test.getAbsolutePath();
//				File file_scores = new File(score_file);
//				String scores_path = file_scores.getAbsolutePath();
//				
//						
//				Execution.logger.debug("Class name: " + "MartReadParameters");
//				Execution.logger.debug("Algorithm: " + algo);
//				Execution.logger.debug("Library: " + library);
//
//				
//				if(library.equals("quickrank")) {
//					
//					arguments_array = new String[3];
//
//					arguments_array[0] = "rm mymodel.xml";
//					arguments = "./bin/quicklearn --train " + train_path + " --valid " + validation_path + " --algo OBVMART" ;
//					
//					//Parameters specific to QuickRank
//					quickrank_train_metric = prop.getProperty("obvmart.quickrank.train-metric");
//					if(quickrank_train_metric!=null) arguments = arguments + " --train-metric " + quickrank_train_metric;
//					quickrank_train_cutoff = prop.getProperty("obvmart.quickrank.train-cutoff");
//					if(quickrank_train_cutoff!=null) arguments = arguments + " --train-cutoff " + quickrank_train_cutoff;
//					quickrank_test_metric = prop.getProperty("obvmart.quickrank.test-metric");
////					if(quickrank_test_metric!=null) arguments = arguments + " --test-metric " + quickrank_test_metric;
//					quickrank_test_cutoff = prop.getProperty("obvmart.quickrank.test-cutoff");
////					if(quickrank_test_cutoff!=null) arguments = arguments + " --test-cutoff " + quickrank_test_cutoff;
//					
//					//Parameters commons to every library
//					num_trees = prop.getProperty("obvmart.num-trees");
//					if(num_trees!=null) arguments = arguments + " --num-trees " + num_trees;
//					shrinkage = prop.getProperty("obvmart.shrinkage");
//					if(shrinkage!=null) arguments = arguments + " --shrinkage " + shrinkage;
//					min_leaf_support = prop.getProperty("obvmart.min-leaf-support");
//					if(min_leaf_support!=null) arguments = arguments + " --min-leaf-support " + min_leaf_support;
//					num_thresholds = prop.getProperty("obvmart.num-thresholds");
//					if(num_thresholds!=null) arguments = arguments + " --num_thresholds " + num_thresholds;
//					estop = prop.getProperty("obvmart.estop");
//					if(estop!=null) arguments = arguments + " --end-after-rounds " + estop;
//					tree_depth = prop.getProperty("obvmart.tree-depth");
//					if(tree_depth!=null) arguments = arguments + " --tree-depth " + tree_depth;
//					
//					arguments = arguments + " --model mymodel.xml";
//					
//					// ./bin/quicklearn --model mymodel.xml --test /.res_TEST.letor --test-metric NDCG --test-cutoff 10 --scores /Users/paolopicello/Documents/workspace/Picello2GUI/resources/scores/quickrank_score_MART.txt
//
//					
//					arguments_array[1] = arguments ;
//					
//					arguments_array[2] = "./bin/quicklearn --model mymodel.xml --test " + test_path + " --test-metric " + quickrank_test_metric + " --test-cutoff " + quickrank_test_cutoff + " --scores " + scores_path;
//					
//					for (int i = 0; i < arguments_array.length; i++) {
//						Execution.logger.debug("Obvmart_QuickRank_Command[" + i + "] = " + arguments_array[i] );
//					}
//				}
//				else {
//					Execution.logger.error("Library name not allowed.");
//				}
//				
//				return arguments_array;
//				
//				
//	}
//
//}
