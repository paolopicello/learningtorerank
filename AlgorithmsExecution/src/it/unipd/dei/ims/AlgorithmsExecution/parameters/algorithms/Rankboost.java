package it.unipd.dei.ims.AlgorithmsExecution.parameters.algorithms;

import java.io.File;
import java.util.Properties;

import it.unipd.dei.ims.AlgorithmsExecution.Execution;

public class Rankboost implements ReadParameters {

	
	String[] arguments_array = null;
	String arguments = "";
	
	String runname = "";
	
	String ranklib_train_metric = "";
	String ranklib_test_metric = "";
	String quickrank_train_metric = "";
	String quickrank_train_cutoff = "";
	String quickrank_test_metric = "";
	String quickrank_test_cutoff = "";
	
	String round = "";
	String tc = "";
	String norm = "";
	
	@Override
	public String[] gen_cmdline(Properties prop, String algo, String library,String runname) {

		// Get my train, validation and test files...
		String train_file = Execution.out_path + "/info/"  + runname + "/"  + runname + "_TRAIN" + ".letor";
		String validation_file =Execution.out_path + "/info/"  + runname + "/" + runname + "_VALIDATION" + ".letor";
		String test_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_TEST" + ".letor";
		String score_file = Execution.out_path + "/info/"  + runname + "/" + runname + "_scores_" + library + ".txt"; 
		
		// Get absolute paths to those files..
		File file_train = new File(train_file);
		String train_path = file_train.getAbsolutePath();
		File file_vali = new File(validation_file);
		String validation_path= file_vali.getAbsolutePath();
		File file_test = new File(test_file);
		String test_path = file_test.getAbsolutePath();
		File file_scores = new File(score_file);
		String scores_path = file_scores.getAbsolutePath();
		
				
		System.out.println("Class name: " + "MartReadParameters");
		System.out.println("Algorithm: " + algo);
		System.out.println("Library: " + library);
		
		if(library.equals("ranklib")) {
			
			arguments_array = new String[2];
			
			arguments = "java -jar bin/" + Execution.ranklib_jar + " -train " + train_path + " -test " + test_path + " -validate " + validation_path +
					" -ranker 2";			
			
			//Parameters specific to Ranklib
			ranklib_train_metric = prop.getProperty("rankboost.ranklib.train-metric");
			if(ranklib_train_metric!=null) arguments = arguments + " -metric2t " + ranklib_train_metric;
			ranklib_test_metric = prop.getProperty("rankboost.ranklib.test-metric");
			if(ranklib_test_metric!=null) arguments = arguments + " -metric2T " + ranklib_test_metric;
			
			//Parameters commons to every library
			round = prop.getProperty("rankboost.round");
			if(round!=null) arguments = arguments + " -round " + round;
			tc = prop.getProperty("rankboost.ranklib.tc");
			if(tc!=null) arguments = arguments + " -tc " + tc;
			norm = prop.getProperty("rankboost.norm");
			if(norm!=null) arguments = arguments + " -norm " + norm;
			arguments = arguments + " -save mymodel.txt";
			
			arguments_array[0] = arguments;
			arguments_array[1] = "java -jar bin/" + Execution.ranklib_jar + " -load mymodel.txt -rank " + test_path + " -score " + scores_path;

			if(Execution.logger.isDebugEnabled()) {
				for (int i = 0; i < arguments_array.length; i++) {
					Execution.logger.debug("Rankboost_Ranklib_Command[" + i + "] = " + arguments_array[i] );
				}
			}
				
		}
		else if(library.equals("quickrank")) {
			arguments_array = new String[3];

			arguments_array[0] = "rm mymodel.xml";
			arguments = "./bin/quicklearn --train " + train_path + " --valid " + validation_path + " --algo RANKBOOST" ;
			
			//Parameters specific to QuickRank
			quickrank_train_metric = prop.getProperty("rankboost.quickrank.train-metric");
			if(quickrank_train_metric!=null) arguments = arguments + " --train-metric " + quickrank_train_metric;
			quickrank_train_cutoff = prop.getProperty("rankboost.quickrank.train-cutoff");
			if(quickrank_train_cutoff!=null) arguments = arguments + " --train-cutoff " + quickrank_train_cutoff;
			quickrank_test_metric = prop.getProperty("rankboost.quickrank.test-metric");
//			if(quickrank_test_metric!=null) arguments = arguments + " --test-metric " + quickrank_test_metric;
			quickrank_test_cutoff = prop.getProperty("rankboost.quickrank.test-cutoff");
//			if(quickrank_test_cutoff!=null) arguments = arguments + " --test-cutoff " + quickrank_test_cutoff;
			round = prop.getProperty("rankboost.round");
			if(round != null) arguments = arguments + " --num-trees " + round;


			arguments = arguments + " --num-trees 10 --model mymodel.xml";
			
			
			arguments_array[1] = arguments;
			
			arguments_array[2] = "./bin/quicklearn --model mymodel.xml --test " + test_path + " --test-metric " + quickrank_test_metric + " --test-cutoff " + quickrank_test_cutoff + " --scores " + scores_path;
			
			if(Execution.logger.isDebugEnabled()) {
				for (int i = 0; i < arguments_array.length; i++) {
					Execution.logger.debug("Rankboost_QuickRank_Command[" + i + "] = " + arguments_array[i] );
				}
			}
		}

		else {
			Execution.logger.error("Library name not allowed.");
		}
		return arguments_array;
	}

}
