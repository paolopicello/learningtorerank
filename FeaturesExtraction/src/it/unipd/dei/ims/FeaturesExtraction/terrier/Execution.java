package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.Vector;

import org.terrier.structures.Index;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import it.unipd.dei.ims.FeaturesExtraction.terrier.Utility;

/**
 * This class controls the execution of the program
 * 
 * @author Paolo Picello
 * @email paolo.picello@studenti.unipd.it
 *
 */


public class Execution {
		
	static String file_properties;
	static String terrier_home;
	static String topicsFile;
	static String runsFolder;
	static String poolFile;
	static String indexPath;
	static String stemmed_string;
	static Index index;
	static String prefix = "data";
	static String table_path;
	static String table_full_path;
	static String pointers_path;
	static String logger_level;
	static String terrier_properties;
	static String runNames[];
	
	static Logger logger;
	
	static Vector<String> topics_text;
	static Vector<String> topics_id;
	
	
	public static void main(String[] args)
			throws IOException, NoSuchMethodException, SecurityException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	     

		logger = Logger.getLogger("execution_logger");
		
		Utility.configureLogger();


		// Properties file not passed as argument.. the default location will be used (resources/config.properties)
		if(args.length < 1) { 
			file_properties = "resources/config.properties";
			logger.warn("Path to properties file not passed as argument.. The default location resources/config.properties will be used");
		}
		else {
			file_properties = args[0]; // first argument is the properties file
		}
		
		long startTime = System.currentTimeMillis();

		/*--------------------------------------------------------------------
		 *  Read properties from file (terrier_home, topics, runsFolder, pool)  
		 *--------------------------------------------------------------------*/
		Properties prop = new Properties();
		InputStream input = null;
		try {

			input = new FileInputStream(file_properties);
			prop.load(input); // load a properties file
			terrier_home = prop.getProperty("terrier_home");
			indexPath = prop.getProperty("index");
			stemmed_string = prop.getProperty("stemmed");
			runsFolder = prop.getProperty("runsFolder");
			poolFile = prop.getProperty("pool");
			topicsFile = prop.getProperty("topics");
			table_path = prop.getProperty("table_path");
			pointers_path = prop.getProperty("pointers_path");
			logger_level = prop.getProperty("logger_level").toUpperCase();
			terrier_properties = prop.getProperty("terrier_properties");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		//----- Define logger level
		Level level = Utility.getLoggerLevel(logger_level); //get logger level from properties file
		logger.setLevel(level);
		//--------------------

		/*--------------------------------------------------------------------
		 *  Read Features from properties and create Features.features
		 *--------------------------------------------------------------------*/
		Features.readFeatures(prop);
		logger.info("Finished to read features from properties");

		
		// prevent potentially expensive computation
		if(logger.isDebugEnabled()) { 
			for (int i = 0; i < Features.features.size(); i++) {
				Features feature = Features.features.get(i);
				logger.debug("FEATURE " + i);
				logger.debug("Class name: " + feature.class_name);
				logger.debug("Data type: " + feature.data_type);
				logger.debug("Field index: " + feature.field_index);
				logger.debug("------------");
			}
		}


		System.setProperty("terrier.home", terrier_home);
		System.setProperty("terrier.setup", terrier_properties); // specify where Terrier finds terrier.properties file
		
		File table_path = new File(Execution.table_path);
		table_full_path = table_path.getAbsolutePath();

		/*---------------------------------------------------------
		 *  Read topics 
		 *---------------------------------------------------------*/
		Topics.extractTopics(); // extract topics (uses Execution.topicsFile)
		logger.info("Finished to read topics from file: " + topicsFile);

		if(logger.isDebugEnabled()) {
			for(int i = 0; i < Topics.topics.size(); i++) {
				Topic topic = Topics.topics.get(i);
				logger.debug("Query id: " + topic.qid + " Text: " + topic.text);
			}
		}
		/*---------------------------------------------------------
		 *  Read pool from file
		 *---------------------------------------------------------*/
		BufferedReader in_pool = new BufferedReader(new FileReader(poolFile));
		Pool.readPool(in_pool); // Read pool from file. Create an hashMap with key in the form "docno/qid" and value rapresenting the relevance
		logger.info("Finished to read pool from file: " + poolFile);

		/*---------------------------------------------------------
		 * Read runs from folder
		 *---------------------------------------------------------*/
		runNames = Utility.listContents(runsFolder);

		/*---------------------------------------------------------
		 *  Load the index
		 *---------------------------------------------------------*/
		index = Index.createIndex(indexPath, prefix); // load an existing index
		
		/*---------------------------------------------------------
		 *  Compute Letor features and populate tables on disk
		 *---------------------------------------------------------*/
		FeaturesExtraction.computeLetor(index, runNames); // main method for letor computation
		
		/*---------------------------------------------------------
		 *  Create read-only properties file in the same folder as tables..
		 *  Prevents accidental modification of this file that could
		 *  break the program
		 *---------------------------------------------------------*/
		Features.createFeatureProperties(prop);


		// Only read operations are allowed on this file 
		File read_only_file = new File(Execution.table_path +  "/features.properties");
		read_only_file.setReadOnly();
		
		
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		double totalTimeSec = (double) totalTime / 1000;
		logger.trace("DONE: " + totalTimeSec);
		
		logger.info("Completed");

	}
}