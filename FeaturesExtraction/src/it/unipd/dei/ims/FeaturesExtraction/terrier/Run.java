package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/** This class parses a particular run file and give the list of DOCNO
 * 
 * @author 	Paolo Picello
 * @email	paolo.picello@studenti.unipd.it
 * 
 */
public class Run {
	
	
	/**
	* This returns a list of docno for a particular qid in a particular file.
	* We can do better, this way we are searching for all even if we know that they are ordered and once i found the first when i found one
	* different we can stop. But doing this we will no longer accept not ordered files. This operation is very fast and not represent a 
	* bottleneck in the execution of the program so this can be avoided.
	* @param in BufferedReader for the file.
	* @param queryID the id of the query.
	* @throws IOException if cannot read the runs.
	*/
	public static List<String> readRunByQueryID (BufferedReader in, int queryID) throws IOException {
		List<String> docnoList = new ArrayList<String>();
		String line = in.readLine();
			
		String[] splitStr = line.split("\\s+");
		int actualQueryID = Integer.parseInt(splitStr[0]);

		while(line != null) {
			splitStr = line.split("\\s+");
			actualQueryID = Integer.parseInt(splitStr[0]);
			
			if(actualQueryID == queryID ) {
				docnoList.add(splitStr[2]);
			}
			line = in.readLine();
		}
		return docnoList;

	}
	
}
