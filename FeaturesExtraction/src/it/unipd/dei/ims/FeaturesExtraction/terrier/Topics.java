package it.unipd.dei.ims.FeaturesExtraction.terrier;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Vector;

import org.terrier.applications.batchquerying.TRECQuery;
import org.terrier.terms.BaseTermPipelineAccessor;
import org.terrier.terms.Stopwords;
import org.terrier.terms.TermPipeline;

/** This class implements a Topic instance with different tags: <NUM>,<TITLE>,<DESC>,<NARR>.
 *  
 * @author 	Paolo Picello
 * @email	paolo.picello@studenti.unipd.it
 * 
 */
class Topic {
	
	String qid;
	String text;

	public Topic(String qid, String text) {
		this.qid = qid;
		this.text = text;
	}
}


/** This class is responsabe for parsing Topics file according to Trec format
 *  
 */
public class Topics {

	static TRECQuery tc;

	static Vector<String> topics_text;
	static Vector<String> topics_id;
	static Stopwords stopw;
	static TermPipeline term ;

	static BaseTermPipelineAccessor tpa;
	static boolean stopwords = false;
	
static 
	List<Topic> topics = new ArrayList<Topic>();

private static Scanner scanner;
  
		/**
		 * Extract topic from TREC File.
		 * N.B. : The terrier.properties have to be in the same foder of the index and have to be named terrier.properties
		 * @param topicsFile full path to topic file
		 * @param indexPath full path of the index
		 * @throws IOException
		 */
	  	public static void extractTopics() throws IOException {
			tc = new TRECQuery();

			topics_text = new Vector<String>();
			topics_id = new Vector<String>();
			tc.extractQuery(Execution.topicsFile, topics_text, topics_id);
			
			String terrier_prop = Execution.terrier_properties;
			Properties prop = new Properties();
			InputStream input = null;
			
			// get the termpipelines string
			input = new FileInputStream(terrier_prop); 
			prop.load(input); 
			String pipeline[] = null;
			
			if(prop.containsKey("termpipelines")) { // terrier prop contiene key termpipelines
				String pipelinestr = prop.getProperty("termpipelines");
				scanner = new Scanner(pipelinestr);
				String word = scanner.next();
				pipeline = word.split(","); // here i have all the terms in termPipeline
				
				if(Execution.logger.isDebugEnabled()) {
					for (int y=0; y<pipeline.length; y++) {
						Execution.logger.debug("term_pipeline: " + pipeline[y]);
					}
				}
				
				// Create a basic term pipeline
				tpa = new BaseTermPipelineAccessor(pipeline);
				String new_query;
				
				for(int i = 0; i < topics_text.size(); i++) {
					new_query = "";
					String elem = topics_text.get(i);
					scanner = new Scanner(elem);
					while(scanner.hasNext()) {
						String queryTerm = scanner.next();
						queryTerm = tpa.pipelineTerm(queryTerm);
						if(queryTerm != null)
							new_query = new_query + queryTerm + " ";
					}
					scanner.close();
					
					Topic topic = new Topic(topics_id.get(i), new_query);
					topics.add(topic);
				}
			}
			else { // properties file not contains termpipeline 
				for( int i = 0; i < topics_id.size(); i++) {
					Topic topic = new Topic(topics_id.get(i), topics_text.get(i));
					topics.add(topic);

				}
			}
			
	  }
	  
}
