package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;

public class VarianceOfTermFrequency extends QueryDependent implements Scoring {

	double sum = 0;
	double mean = 0;
	double variance = 0;
	
	public VarianceOfTermFrequency (Index index, int docid,int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid,field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		
		// Compute mean 
		for(int i = 0; i < termFreqs.length; i++) {
			 sum = sum + termFreqs[i];			
		}
		
		mean = sum / termFreqs.length;
		
		// sum_over_i [(tf[i]-mean)^2]
		for(int i = 0; i < termFreqs.length; i++) {
			variance = variance + Math.pow((termFreqs[i]-mean),2);
		}

		return variance;

	}

}