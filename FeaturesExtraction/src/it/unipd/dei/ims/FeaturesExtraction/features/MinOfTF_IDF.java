package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;


/**
 * TF = n. occorrenze del termine nel doc/dimensione doc.
 * IDF = log(n. doc in collezione/n. doc che contengono termine ti) 
 * TF_IDF = TF * IDF
 * 
 * @author paolopicello
 *
 */
public class MinOfTF_IDF extends QueryDependent implements Scoring {

	int docLength;
	int totalDocs;

	
	public MinOfTF_IDF (Index index, int docid,int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid,field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		
		double minOfTF_IDF = 1000;

		for(int i = 0; i < tf_idf.length; i++) {
		
			if(tf_idf[i] < minOfTF_IDF ) minOfTF_IDF = tf_idf[i];		
		}
		
		return minOfTF_IDF;
	}

}