package it.unipd.dei.ims.FeaturesExtraction.features;

import org.terrier.structures.Index;


/**
 * TF = n. occorrenze del termine nel doc/dimensione doc.
 * IDF = log(n. doc in collezione/n. doc che contengono temrine ti) 
 * TF_IDF = TF * IDF
 * 
 * @author paolopicello
 *
 */
public class MaxOfTF_IDF extends QueryDependent implements Scoring {

	int docLength;
	int totalDocs;

	
	public MaxOfTF_IDF (Index index, int docid, int field, String[] terms, double[] termFreqs, double[] tf_idf) {
		super(index,docid,field, terms,termFreqs, tf_idf);
	}
	
	@Override
	public double score() {
		
		double maxOfTF_IDF = 0;
		
		// for each term compute TF_IDF e get the maximum
		for(int i = 0; i < tf_idf.length; i++) {
	
			if(tf_idf[i] > maxOfTF_IDF) maxOfTF_IDF = tf_idf[i];	
		
		}
		
		return maxOfTF_IDF;
	}

}